# Welcome To The A-Sharp Energy Front End Exploration #

This explanation is meant to give you a chance to try out our front end, and us a chance to see some of your work.

### Set Up ###

You don't need to install or use Rails to engage in this exploration. You'll only need a little bit of CSS, Bootstrap, and Haml.

1. Verify that you have a version of ruby installed by typing `ruby -v` in a console. If not, install a version of ruby.
1. Install Haml with `gem install haml`
1. Clone this repository with `git clone git@bitbucket.org:pkirwin/asharpenergy_frontend.git`
    * You need to have an account with bitbucket to clone the repo.
    * Your bitbucket account needs to have an ssh-key associated with your computer. Bitbucket has helpful tutorials to help you do this if you haven't done it before.
1. Open the web page by visiting 'file:///...route to folder containing this repo.../front_end_test.html' in a browser window. You don't need to have a local server running or anything like that.
1. If you change application.css, you need only refresh the browser window.
1. If you change front_end_test.html.haml, you need to run `ruby generate_html_file.rb` in the folder containing the repo before you refresh the browser window (this builds the html document front_end_test.html from the haml document front_end_test.html.haml).

### Wrapping Up ###

When you've finished this exploration, you can push a new branch to this repository. Please name this branch with your names in a first_last format. For example, I would name my branch peter_kirwin.

Let Peter know after you push your branch, both so that we can see your great work, but also so that we can restrict access to it.

### About The CSS File ###

* You'll notice that CSS file is not very well organized; I recommend just searching for the tags you want to change.
* The CSS file is organized -- and this is about where the organization ends -- according to media size, with the biggest media sizes at the top and increasingly smaller media sizes further down the file -- a quick search for `@media` will get you right to the boundaries of those media sizes.

### What Do We Want To See? ###

In particular, we're looking for a few items:

* The image should be on top of the text instead of below it for screen sizes when the text and image stack.
* The image should have constant dimensions within each of the media size thresholds. Right now it grows and shrinks dynamically at medium screen sizes, which we don't like.
* Bonus points for adding a little bit of space between the colored numbers and the text that succeeds them.

But we're generally looking for this web page to look and feel pleasant on a variety of screen sizes. So feel free to make any improvements you see think improve the experience.

### About This Exploration ###

* Once you get the components working together, please only spend up to 2 hours working on it. We'll pay you $20/hr ($40 total) for participating in this exploration.
* Let Peter know if you have any issues getting the set up working.
* You may not have the page in a completely finished state at the end of the 2 hours. That's fine; the amount of work and the 2 hour time window are not perfectly calibrated. Just get done what you can within that time frame.
* Once all of the participants have completed the exploration, we'll select a few to meet the technical and design team. Then we'll make a decision.
* Good luck, and have fun!